
# Birthday helper! Write a program to read in names and birth dates from a text
# file. It should then ask you for a name. You type one in, and it tells you
# when that person’s next birthday will be (and, for the truly adventurous, how
# old they will be). The input file should look something like this:
#
# => Christopher Alexander, Oct 4, 1936
# => Christopher Lambert, Mar 29, 1957
# => Christopher Lee, May 27, 1922
# => Christopher Lloyd, Oct 22, 1938
# => Christopher Pine, Aug 3, 1976
# => Christopher Plummer, Dec 13, 1927
# => Christopher Walken, Mar 31, 1943
# => The King of Spain, Jan 5, 1938
#
# (That would be me Christopher Pine, not that other guy who comes before me on
# Google....) You’ll probably want to break each line up and put it in a hash,
# using the name as your key and the date as your value; in other words:
#
#  birth_dates[ 'The King of Spain' ] = 'Jan 5, 1938'
#
# (although you can store the date in some other format if you prefer).
# Though you can do it without this tip, your program might look prettier if you
# use the each_line method for strings. It works pretty much like each does for
# arrays, but it returns each line of the multiline string one at a time (but
# with the line endings, so you might need to chomp them). Just thought I’d
# mention it....

# Holds the pathname to the file that contains the people data.
DATA = 'birth_dates.cvs'

# Calculates the days left to a given date from a starting date.
#
# start_date  - Time object of date that starts the time lapse.
# target_date - Time object of date that ends the time lapse.
#
# Precondition: start_date <= target_date
#
# Examples
#
#    start_date  = Time.new 2014, 9, 30
#    target_date = Time.new 2014, 9, 30
#    days_to_date(start_date, target_date)
#    # => 0
#
#    start_date  = Time.new 1981, 8, 6
#    target_date = Time.new 2014, 9, 30
#    days_to_date(start_date, target_date)
#    # => 12108
#
# Returns the number of days left to the date as an Integer.
def days_to_date(start_date, target_date)
  difference = target_date - start_date
  difference < 86400 ? 0 : (difference / 86400).to_i
end

# Calculates the consecutive number of an anniversary celebration.
#
# start_date  - Time object of the date to be remembered.
# anniversary - Time object of the anniversary date whose consecutive number
#               wants to be known.
#
# Precondition: start_date <= anniversary
#
# Examples
#
#    start_date  = Time.new 2014, 9, 30
#    anniversary = Time.new 2014, 9, 30
#    anniversary_number(start_date, anniversary)
#    # => 0
#
#    start_date  = Time.new 1981, 8, 6
#    anniversary = Time.new 2014, 8, 6
#    anniversary_number(start_date, anniversary)
#    # => 33
#
# Returns the consecutive number for the anniversary as an Integer.
def anniversary_number(start_date, anniversary)
  anniversary.year - start_date.year
end

# From an anniversary, it gets the closer celebration date to a given date.
#
# date        - Time object of the date closer to the anniversary.
# anniversary - Time object of the original event's date.
#
# Examples
#
#    date        = Time.new 2014, 9, 30
#    anniversary = Time.new 1981, 8,  6
#    next_celebration(date, anniversary)
#    # => 2015-08-06 00:00:00 -0500
#
#    date        = Time.new 1983, 11, 7
#    anniversary = Time.new 2014,  8, 6
#    next_celebration(date, anniversary)
#    # => 2014-11-07 00:00:00 -0600
#
# Returns the Time object of the closer celebration date of the anniversary.
def next_celebration(date, anniversary)
  maybe = Time.new(date.year, anniversary.month, anniversary.day)
  date < maybe ? maybe : Time.new(date.year + 1, maybe.month, maybe.day)
end

# Main interactive program function.
def birthday_helper
  def load_data
    [].tap do |data|
      File.open(DATA, 'r') { |f| f.each_line { |line| data << line.chomp } }
    end
  end

  def prepare_data(persons)
    {}.tap do |data|
      persons.each do |person|
        name, details, year = person.split(', ')
        data[name] = { year: year }

        month, day = details.split
        data[name][:month] = month
        data[name][:day  ] = day
      end
    end
  end

  def list(persons)
    persons.each do |name, data|
      puts "#{name}: #{data[:month]} #{data[:day]}, #{data[:year]}"
    end
  end

  def details(name, data)
    # Set the context data
    today = Time.now
    birth = Time.new data[:year], data[:month], data[:day]

    # Get the info for the next celebration
    next_birthday   = next_celebration(today, birth)
    days_left       = days_to_date(today, next_birthday)
    birthday_number = anniversary_number(birth, next_birthday)

    # Display data
    puts
    puts 'Lets, see...'

    if days_left > 0
      puts "There are #{days_left} day(s) left for #{name}'s birthday."
      puts "He/She will be #{birthday_number} at that time."
    else
      puts "Today is #{name}'s birthday!"
      puts "He/She will be #{birthday_number} today. Be sure to celebrate. :)"
    end

    puts
  end

  # Load and format data
  persons = prepare_data(load_data)

  while true
    puts  'Type the name of the person you want to know his/her birthday.'
    puts  '«list» shows the available names. «exit» quits the program.'
    print 'Option: '
    option = gets.chomp

    case option
    when 'exit'
      puts '¡Adios!'
      break
    when 'list'
      list persons
    else
      if persons.key? option
        details option, persons[option]
      else
        puts 'Unknown Option or Name.'
      end
    end
  end
end

birthday_helper
