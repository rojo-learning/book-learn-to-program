
# Time to party like it’s roman_to_integer 'mcmxcix'! Come on, you knew it was
# coming, didn’t you? It’s the other half of your roman_to_numeral 1999 method.
# Make sure to reject strings that aren’t valid Roman numerals.

# Transforms a «small» Roman numeral into its Arabic integer version, checing
# that the received string is a valid Roman numeral.
#
# roman - Strign representing a Roman numeral.
#
# Examples
#
#   roman_to_integer('IX')
#   # => 9
#   roman_to_integer('MCMXCIX')
#   # => 1999
#   roman_to_integer('LXVS')
#   # => "LXVS is not a valid Roman numeral."
#
# Returns the integer representation of the given Roman numeral.
def roman_to_integer(roman)
  # Current configuration should make possible to take numbers up to the
  # theoretical limit of 3999 as valid numerals, but it may permit bigger
  # numbers ajusting the 'max limit' in the «numerals» hash below.
  numerals = {
    'I' => { 'value' =>    1, 'precursors' => 'MDCLXVI', 'subtractor' =>  '',
             'max limit' => 4, 'count' => 0, 'max subtractions' => 1        ,
             'subtractions' => 0                                           },
    'V' => { 'value' =>    5, 'precursors' => 'MDCLXI' , 'subtractor' => 'I',
             'max limit' => 1, 'count' => 0, 'max subtractions' => 0        ,
             'subtractions' => 0                                           },
    'X' => { 'value' =>   10, 'precursors' => 'MDCLXI' , 'subtractor' => 'I',
             'max limit' => 4, 'count' => 0, 'max subtractions' => 1        ,
             'subtractions' => 0                                           },
    'L' => { 'value' =>   50, 'precursors' => 'MDCX'   , 'subtractor' => 'X',
             'max limit' => 1, 'count' => 0, 'max subtractions' => 0        ,
             'subtractions' => 0                                           },
    'C' => { 'value' =>  100, 'precursors' => 'MDCX'   , 'subtractor' => 'X',
             'max limit' => 4, 'count' => 0, 'max subtractions' => 1        ,
             'subtractions' => 0                                           },
    'D' => { 'value' =>  500, 'precursors' => 'MDC'    , 'subtractor' => 'C',
             'max limit' => 1, 'count' => 0, 'max subtractions' => 0        ,
             'subtractions' => 0                                           },
    'M' => { 'value' => 1000, 'precursors' =>  'MC'    , 'subtractor' => 'C',
             'max limit' => 4, 'count' => 0, 'max subtractions' => 0        ,
             'subtractions' => 0                                           }
  }

  # Identifies most of valid Roman numerals.
  def valid?(letter, previous, numerals, subtraction)
    numeral   = numerals[letter]

    # Rule out non-valid characters.
    return false if numeral == nil

    unless previous == nil
      # Rule out non-valid precursors.
      return false unless numeral['precursors'].include? previous

      #Rule out multiple uses of a numeral as subtractor.
      precursor = numerals[previous]
      if subtraction
        if precursor['max subtractions'] == precursor['subtractions']
          return false
        end
      end
    end

    # Rule out excesively used numerals.
    numeral['count'] += 1
    return false if numeral['count'] > numeral['max limit']

    true
  end

  # Gets the integer value of the Roman numeral, taking its predecesor into
  # account.
  def get_value(letter, previous, numerals, subtraction)
    numeral = numerals[letter]
    return numeral['value'] if previous == nil

    if subtraction
      numerals[previous]['subtractions'] += 1
      numeral['value'] - numerals[previous]['value'] * 2
    else
      numeral['value']
    end
  end

  def will_subtract?(letter, previous, numerals)
    unless numerals[letter] == nil
      if numerals[letter]['subtractor'] == previous
        return true
      end
    end

    false
  end

  previous = nil
  arabic   = 0

  roman.length.times do |index|
    subtraction = will_subtract? roman[index], previous, numerals

    if valid? roman[index], previous, numerals, subtraction
      arabic  += get_value roman[index], previous, numerals, subtraction
      previous = roman[index]
    else
      return roman + ' is not a valid Roman numeral.'
    end
  end

  # Last test: Rule out numerals with a value bigger than the 'max limit' for M.
  if arabic <= numerals['M']['value'] * numerals['M']['max limit']
    arabic
  else
    roman + ' is not a valid Roman numeral.'
  end
end

puts roman_to_integer('LXVS')      # => 'LXVS is not a valid Roman numeral.'
puts roman_to_integer('LVVI')      # => 'LVVI is not a valid Roman numeral.'
puts roman_to_integer('ILXV')      # => 'ILXV is not a valid Roman numeral.'
puts roman_to_integer('IXIV')      # => 'IXIV is not a valid Roman numeral.'
puts roman_to_integer('MMMMI')     # => 'MMMMI is not a valid Roman numeral.'
puts roman_to_integer('IX')        # => 9
puts roman_to_integer('MCMXCIX')   # => 1999
puts roman_to_integer('MMMCMXCIX') # => 3999
