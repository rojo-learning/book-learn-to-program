
# How about making your shuffle method on page 85 an array method? Or making
# factorial an integer method? 4.to_roman, anyone? In each case, remember to use
# self to access the object the method is being called on (the 4 in 4.to_roman).

class Array
  def suffle
    def remove_element(element, list)
      helper = []
      index  = 0
      limit  = list.length

      # Cycle through «list», pushing every element into the «helper», except
      # for the «element» element.
      while index < limit
        if list[index] == element
          element == nil
        else
          helper.push list[index]
        end

        index = index + 1
      end

      helper
    end

    list    = self
    suffled = []

    # Cycle while the «list» has elements, picking a random «element» each time.
    # That «element» is appended to the «suffled» version and is removed from the
    # «list».
    while list.length > 0
      if list.length == 1
        element = list[0]
      else
        element = list[rand(list.length)]
      end

      suffled.push(element)
      list = remove_element(element, list)
    end

    suffled
  end
end

10.times { p (0..9).to_a.suffle }
