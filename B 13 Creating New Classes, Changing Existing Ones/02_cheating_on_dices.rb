
# Our dice are just about perfect. The only feature that might be missing is a
# way to set which side of a die is showing...why don’t you write a cheat method
# that does just that? Come back when you’re done (and when you tested that it
# worked, of course). Make sure that someone can’t set the die to have a 7
# showing; you’re cheating, not bending the laws of logic.

class Die
  def initialize
    roll
  end

  def roll
    @face = rand(6) + 1
  end

  def face
    @face
  end

  def cheat(number)
    if number < 1 or number > 6
      # You've tried to bend reality,
      # now reality may bend you.
      roll
    else
      @face = number
    end
  end
end

die = Die.new
puts 'Die initial face'
puts die.face
puts 'Rolling the die'
puts die.roll
puts 'Cheating with the max (6)'
puts die.cheat(6)
puts 'How about going a little further? (9)'
puts die.cheat(9)
