
# Build your own playlists! For this to work, you need to have some music ripped
# to your computer in some format. We’ve ripped a 100 or so CDs, and we keep
# them in directories something like
#
#   music/genre/artist_and_cd_name/track_number.ogg
#
# (I’m partial to the .ogg format, though this would work just as well with
# .mp3s or whatever you use.)
#
# Building a playlist is easy. It’s just a regular text file (no YAML required,
# even). Each line is a filename, like
#
#   music/world/Stereolab--Margarine_Eclipse/track05.ogg
#
# What makes it a playlist? Well, you have to give the file the .m3u extension,
# like playlist.m3u or something. And that’s all aplaylist is: a text file with
# a .m3u extension. So have your program search for various music files and
# build you a playlist. Use your shuffle method on page 85 to mix up your
# playlist. Then check it out in your favorite music player (Winamp, MPlayer,
# etc.)!

########
# Code rehused from a previus program...

# Searches for the given element (number or string) into the given list of items
# of the same type, if it exists, returns a version of the list without that
# element.
#
# element - Number or string.
# list    - Array of numbers or strings.
#
# Examples
#
#   remove_element(2, [1, 2, 3])
#   # => [1, 3]
#   remove_element(4, [1, 2, 3])
#   # => [1, 2, 3]
#
# Returns the given array without the given element.
def remove_element(element, list)
  helper = []
  index  = 0
  limit  = list.length

  # Cycle through «list», pushing every element into the «helper», except
  # for the «element» element.
  while index < limit
    if list[index] == element
      element == nil
    else
      helper.push list[index]
    end

    index = index + 1
  end

  helper
end

# Creates a suffled version of the given list that contains numbers or strings.
#
# list - Array of numbers or strings.
#
# Examples
#
#   suffle([1, 2, 3, 4, 5])
#   # => [2, 4, 1, 5, 3]
#   suffle([1, 2, 3, 4, 5])
#   # => [1, 5, 3, 4, 2]
#
# Returns a suffled version of the given array.
def suffle(list)
  suffled = []

  # Cycle while the «list» has elements, picking a random «element» each time.
  # That «element» is appended to the «suffled» version and is removed from the
  # «list».
  while list.length > 0
    if list.length == 1
      element = list[0]
    else
      element = list[rand(list.length)]
    end

    suffled.push(element)
    list = remove_element(element, list)
  end

  suffled
end

########
# For this program I've created a structure of dummy media files under the
# music/ directory. The program should read all the files from the directories
# pending from it and create a ramdom playlist file under the playlists/
# directory.

ini_path   = 'music/**/'       # Path were the media files will be searched.
end_path   = 'playlists/'      # Path were the file will be placed.
file_ext   = 'mp3,aac,ogg,wav' # File extensions to be searched.
new_ext    = "m3u"             # New file extension for the created file.
file_name  = 'random'          # Name for the created file.

# Get the file names with «file_ext» found in «ini_path».
songs = Dir[ini_path + '*.{' + file_ext +'}']

if songs.length == 0
  # If no files are found...
  puts "No music files were found in " + ini_path
else
  print "Creating playlist"

  # Suffle the retrieved songs list.
  songs = suffle(songs)

  # Open (creating it if doen's exist) the playlist file.
  playlist = end_path + file_name + '.'+ new_ext
  File.open(playlist, "w") do | f |
    songs.each do | song |
      print '.'
      f.write song + "\n" # f.puts can be used instead
    end
  end

  puts
  puts "Your new playlist can be found in " + playlist + ". Enjoy! :)"
end
