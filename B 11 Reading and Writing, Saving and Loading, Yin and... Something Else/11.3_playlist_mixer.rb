
# Build better playlists! After listening to playlists for a while, you might
# start to find that a purely random shuffle just doesn’t quite seem... mixed up
# enough. (Random and mixed up are not at all the same thing. Random is totally
# clumpy.) For example, here’s an excerpt from a playlist I made a while back of
# Thelonius Monk and Faith No More:
#
#  music/Jazz/Monk--Nutty/track08.ogg
#  music/Jazz/Monk--London_Collection_1/track05.ogg
#  music/Jazz/Monk--Nutty/track13.ogg
#  music/Jazz/Monk--Round_Midnight/track02.ogg
#  music/Jazz/Monk--Round_Midnight/track14.ogg
#  music/Jazz/Monk--Round_Midnight/track15.ogg
#  music/Jazz/Monk--Round_Midnight/track08.ogg
#  music/Rock/FNM--Who_Cares_A_Lot_2/track02.ogg
#  music/Rock/FNM--Who_Cares_A_Lot_2/track08.ogg
#  music/Rock/FNM--Who_Cares_A_Lot_1/track02.ogg
#  music/Rock/FNM--Who_Cares_A_Lot_2/track01.ogg
#
# Hey! I asked for random! Well, that’s exactly what I got...but I wanted mixed
# up. So here’s the grand challenge: instead of using your old shuffle , write a
# new music_shuffle method. It should take an array of filenames (like those
# listed previously) and mix them up good and proper.
#
# You’ll probably need to use the split method for strings. It returns an array
# of chopped-up pieces of the original string, split where you specify, like
# this:
#
#  awooga = 'this/is/not/a/daffodil'.split '/'
#  puts awooga
#  this
#  is
#  not
#  a
#  daffodil
#
# Mix it up as best you can!

########
# Code rehused from a previus program...

# Searches for the given element (number or string) into the given list of items
# of the same type, if it exists, returns a version of the list without that
# element.
#
# element - Number or string.
# list    - Array of numbers or strings.
#
# Examples
#
#   remove_element(2, [1, 2, 3])
#   # => [1, 3]
#   remove_element(4, [1, 2, 3])
#   # => [1, 2, 3]
#
# Returns the given array without the given element.
def remove_element(element, list)
  helper = []
  index  = 0
  limit  = list.length

  # Cycle through «list», pushing every element into the «helper», except
  # for the given «element».
  while index < limit
    if list[index] == element
      element == nil
    else
      helper.push list[index]
    end

    index = index + 1
  end

  helper
end

# Creates a suffled version of the given list that contains numbers or strings.
#
# list - Array of numbers or strings.
#
# Examples
#
#   suffle([1, 2, 3, 4, 5])
#   # => [2, 4, 1, 5, 3]
#   suffle([1, 2, 3, 4, 5])
#   # => [1, 5, 3, 4, 2]
#
# Returns a suffled version of the given array.
def suffle(list)
  suffled = []

  # Cycle while the «list» has elements, picking a random «element» each time.
  # That «element» is appended to the «suffled» version and is removed from the
  # «list».
  while list.length > 0
    if list.length == 1
      element = list[0]
    else
      element = list[rand(list.length)]
    end

    suffled.push(element)
    list = remove_element(element, list)
  end

  suffled
end

########
# For this program I've created a structure of dummy media files under the
# music/ directory. The program should read all the files from the directories
# pending from it and create a randomly mixed play-list file under the playlists/
# directory.
#
# As mr. Pine did not made himself clear about the meaning of «mixing them up
# good and proper», I took some liberties with the program. Instead of creating
# a completely random play-list, the program now stacks one song of each artist
# in the list up to a set number of songs. If the set number has been not
# reached, it will take one more song of each of the available artists,
# following the same artist order. The order of the artists and of the songs
# available is randomized at the start.

# Selects from a list of given path-files to songs the ones interpreted by a
# given artist.
#
# songs    - String array containing the songs path name.
# artist   - String of the artist name.
# position - Integer that indicates the position of the artists names in the
#            path-file string once split.
#
# Examples
#
#   songs = [
#     'music/hip-hop/Black Eyed Peas/Monkey Business/Union.mp3',
#     'music/psychedelic/Tame Impala/Innerspeaker/Lucidity.mp3'
#   ]
#   get_artist_songs(songs, "Tame Impala", 2)
#   # => ['music/psychedelic/Tame Impala/Innerspeaker/Lucidity.mp3']
#
# Returns an array of strings that represent the matching path-files of the
# artist songs.
def get_artist_songs(songs, artist, position)
  artist_songs = []

  songs.each do | song |
    if song.split('/')[position] == artist
      artist_songs.push song
    end
  end

  artist_songs
end

# Extracts the list of artists from an array containing strings that represent
# the path-file of a collection of songs.
#
# songs    - String array containing the songs path name.
# position - Integer that indicates the position of the artists names in the
#            path-file string once split.
#
# Examples
#
#   songs = [
#     'music/hip-hop/Black Eyed Peas/Monkey Business/Union.mp3',
#     'music/psychedelic/Tame Impala/Innerspeaker/Lucidity.mp3'
#   ]
#   get_artists(songs, 2)
#   # => ['Black Eyed Peas', 'Tame Impala']
#
# Returns an array of the available artists names as strings.
def get_artists(songs, position)
  artists = []

  songs.each do | song |
    new_artist = song.split('/')[position]

    # Check if the artist is already in the array.
    duplicate = false
    artists.each do | artist |
      if new_artist == artist
        duplicate = true
      end
    end

    if not duplicate
      artists.push new_artist
    end
  end

  artists
end

# Mixes a given list of songs, trying to balance the amount of songs of each
# artist until a given quantity of songs is reached.
#
# songs    - String array containing the songs path name.
# quantity - Integer that limits the number of songs included in the play-list.
#            If the quantity is bigger that the number of songs available, the
#            play-list will have a much songs as available.
#
# Returns an array of randomly-mixed path-files to music represented with
# strings.
def mix(songs, quantity)
  # Get and randomly sort the artists available.
  # For this program, the artist position in the path-file is hard-coded.
  artists    = suffle get_artists(songs, 2)

  # Built an array of ['artist', ['songs']] to select the songs for the mix.
  mixed_data = []
  artists.each do | artist |
    # Get and randomly sort the available songs of the artist.
    artist_songs = suffle get_artist_songs(songs, artist, 2)
    mixed_data.push [artist, artist_songs]
  end

  selected_songs = []
  while selected_songs.length < quantity and mixed_data.length > 0
    # Select a song for every artist in the mixed data.
    mixed_data.each do | artist_data |
      selected_songs.push artist_data[1].pop
    end

    # Mark every artist that doesn't have songs left.
    tainted_artists = []
    mixed_data.each do | artist_data |
      if artist_data[1].length == 0
        tainted_artists.push artist_data
      end
    end

    # Remove the marked artists.
    tainted_artists.each do | artist_data |
      mixed_data = remove_element artist_data
    end
  end

  # Remove songs to fit the desired quantity if the list length is bigger.
  if selected_songs.length > quantity
    (selected_songs.length - quantity).times do
      selected_songs.pop
    end
  end

  selected_songs
end

# Main block
ini_path   = 'music/**/'       # Path were the media files will be searched.
end_path   = 'playlists/'      # Path were the file will be placed.
file_ext   = 'mp3,aac,ogg,wav' # File extensions to be searched.
new_ext    = "m3u"             # New file extension for the created file.
file_name  = 'mixed'           # Name for the created file.

# Get the file names with «file_ext» found in «ini_path».
puts "* Loading songs"
songs = Dir[ini_path + '*.{' + file_ext + '}']

if songs.length == 0
  # If no files are found...
  puts "No music files were found in " + ini_path
else
  print "* Creating play-list"

  # Shuffle and mix the retrieved songs list.
  songs = mix(songs, 10)

  # Open (creating it if doesn't exist) the play-list file.
  playlist = end_path + file_name + '.'+ new_ext
  File.open(playlist, "w") do | f |
    songs.each do | song |
      print '.'
      f.puts song
    end
  end

  puts
  puts "Your new play-list can be found in " + playlist + ". Enjoy! :)"
end
