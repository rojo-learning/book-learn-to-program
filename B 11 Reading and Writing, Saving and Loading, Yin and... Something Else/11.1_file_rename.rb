
# Adapt the picture-downloading/file-renaming program to your computer. Add some
# safety features to make sure you never overwrite a file. A few methods you
# might find useful are File.exist? (pass it a filename, and it will return true
# or false ) and exit (like if return and Napoleon had a baby —it kills your
# program right where it stands; this is good for spitting out an error message
# and then quitting).


########
# Well, my program does not exit if it fails to move a file because it already
# exists a file with that name. Rather, the programs continues with other files,
# saving the names of the ones that could not be renamed due that reason and
# displays the names at the end.

ini_path   = 'ini/'    # Path were the files will be searched.
end_path   = 'end/'    # Path were the files will be placed.
file_ext   = 'txt,TXT' # File extensions to be searched.
new_ext    = "txt"     # New file extension for the files.
batch_name = 'TEST'    # Batch name for the files.

# Get the file names with «file_ext» found in «ini_path».
files  = Dir[ini_path + '*.{' + file_ext +'}']
number = 1

if files.length == 0
  # If no files are found...
  puts "No ." + file_ext + " files were found in " + ini_path
else
  puts "Renaming " + files.length.to_s + " files"
  already_exist = []

  files.each do | file_name |

    if number < 10
      new_name = end_path + batch_name + "0" + number.to_s + '.' + new_ext
    else
      new_name = end_path + batch_name + number.to_s + '.' + new_ext
    end

    if not File.exist? new_name
      # COMMENT/UNCOMMENT the next line to review/do file changes.
      # File.rename file_name, new_name
      print '.' # Mark success.
    else
      # If the file already exists, save its name.
      already_exist.push new_name
      print "x" # Mark failure.
    end

    number = number + 1
  end

  puts

  # Display which files could not be renamed because there is already a file
  # with that name in the «end_path».
  already_exist.each do | file_name |
    puts "The file " + file_name + " already exists."
  end

end
