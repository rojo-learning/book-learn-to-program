
# Old-school Roman numerals. In the early days of Roman numerals, the Romans
# didn’t bother with any of this new-fangled subtraction “IX” nonsense. No sir,
# it was straight addition, biggest to littlest —so 9 was written “VIIII,” and
# so on. Write a method that, when passed an integer between 1 and 3000 (or so),
# returns a string containing the proper old-school Roman numeral. In other
# words, old_roman_numeral 4 should return ' IIII '. Make sure to test your
# method on a bunch of different numbers. Hint: Use the integer division and
# modulus methods on page 36.
#
# For reference, these are the values of the letters used:
# I = 1, V = 5, X = 10, L = 50, C = 100, D = 500, M = 1000

def toRomanNumeral number
  # Array of arrays where each array contains the pair value-letter.
  letters = [
    [1000, 'M'], [500, 'D'], [100, 'C'], [50, 'L'], [10, 'X'], [5, 'V'],
    [   1, 'I']
  ]

  # The string that will be returned.
  numeral = ''
  # A copy of the number for manipulation.
  left    = number

  # Process each letter iteratively:
  letters.each do | letter |
    # Test the letter to see if it fits in «left».
    if (left / letter[0]) >= 1
      # Get the number of times the letter fits in «left».
      write   = left / letter[0]
      # Subtract the equivalent value from «left».
      left    = left - write * letter[0]
      # Append the letter to «numeral» the number of times in «write».
      numeral = numeral + letter[1] * write
    end
  end

  numeral
end

puts toRomanNumeral 1
puts toRomanNumeral 2
puts toRomanNumeral 5
puts toRomanNumeral 6
puts toRomanNumeral 10
puts toRomanNumeral 11
puts toRomanNumeral 15
puts toRomanNumeral 16
puts toRomanNumeral 20
puts toRomanNumeral 49
puts toRomanNumeral 50
puts toRomanNumeral 55
puts toRomanNumeral 57
puts toRomanNumeral 100
puts toRomanNumeral 199
puts toRomanNumeral 500
puts toRomanNumeral 555
puts toRomanNumeral 1000
puts toRomanNumeral 2343
puts toRomanNumeral 3000
