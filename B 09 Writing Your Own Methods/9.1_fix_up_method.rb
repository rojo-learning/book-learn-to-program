
# Fix up the ask method. That ask method I showed you was alright, but I bet you
# could do better. Try to clean it up by removing the variables good_answer and
# answer. You’ll have to use return to exit from the loop. (Well, it will get
# you out of the whole method, but it will get you out of the loop in the
# process.) How do you like the resulting method? I usually try to avoid using
# return (personal preference), but I might make an exception here.

def ask question

  while true
    puts question
    reply = gets.chomp.downcase

    if reply == 'yes'
      return true
    elsif reply == 'no'
      return false
    else
      puts 'Please answer "yes" or "no".'
    end
  end

end

puts ask "Dou you like dorilocos?"
