
# “Modern” Roman numerals. Eventually, someone thought it would be terribly
# clever if putting a smaller number before a larger one meant you had to
# subtract the smaller one. As a result of this development, you must now
# suffer. Rewrite your previous method to return the new-style Roman numerals,
# so when someone calls roman_numeral 4 , it should return 'IV'.

# Wikipedia says the following about subtractive notation in Roman numerals:
# «in a few specific cases, to avoid four characters being repeated in
# succession (such as IIII or XXXX) these can be reduced using subtractive
# notation as follows:
#   * the numeral I can be placed before V and X to make 4 units (IV) and 9
#     units (IX respectively).
#   * X can be placed before L and C to make 40 (XL) and 90 (XC respectively).
#   * C can be placed before D and M to make 400 (CD) and 900 (CM) according to
#     the same pattern.

def toRomanNumeral number
  # Array of arrays where each array contains the pair value-letter.
  letters = [
    [1000, 'M'], [500, 'D'], [100, 'C'], [50, 'L'], [10, 'X'],
    [5, 'V'], [   1, 'I']
  ]

  # The string that will be returned.
  numeral = ''
  # A copy of the number for manipulation.
  left    = number

  # Process each letter iteratively:
  letters.each do | letter |
    if (left / letter[0]) >= 1
      # Check if the number can be reduced with the given rules.
      if    left >= 900 and left <= 999
        numeral = numeral + 'CM'
        left    = left - 900
      elsif left >= 400 and left <= 499
        numeral = numeral + 'CD'
        left    = left - 400
      elsif left >=  90 and left <=  99
        numeral = numeral + 'XC'
        left    = left - 90
      elsif left >=  40 and left <=  49
        numeral = numeral + 'XL'
        left    = left - 40
      elsif left == 9
        numeral = numeral + 'IX'
        left    = 0
      elsif left == 4
        numeral = numeral + 'IV'
        left    = 0
      else
      # If the number can't be reduced, proceed normaly.
        # Get the number of times the letter fits in «left».
        write   = left / letter[0]
        # Subtract the equivalent value from «left».
        left    = left - write * letter[0]
        # Append the letter to «numeral» the number of times in «write».
        numeral = numeral + letter[1] * write
      end
    end
  end

  numeral
end

# Normal test cases
puts toRomanNumeral 1
puts toRomanNumeral 2
puts toRomanNumeral 5
puts toRomanNumeral 6
puts toRomanNumeral 10
puts toRomanNumeral 11
puts toRomanNumeral 15
puts toRomanNumeral 16
puts toRomanNumeral 20
puts toRomanNumeral 49
puts toRomanNumeral 50
puts toRomanNumeral 55
puts toRomanNumeral 57
puts toRomanNumeral 100
puts toRomanNumeral 199
puts toRomanNumeral 500
puts toRomanNumeral 555
puts toRomanNumeral 1000
puts toRomanNumeral 2343
puts toRomanNumeral 3000

# Special test cases
puts toRomanNumeral 4
puts toRomanNumeral 9
puts toRomanNumeral 40
puts toRomanNumeral 90
puts toRomanNumeral 400
puts toRomanNumeral 900
puts toRomanNumeral 1904
puts toRomanNumeral 1954
puts toRomanNumeral 1990
puts toRomanNumeral 2014
