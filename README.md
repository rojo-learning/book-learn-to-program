
Missing exercises from «Learn to Program»
=========================================
This repository contains answers to to the programs suggested as exercises
from the book [_Learn to Program_ by Chris Pine][2], but only the ones that do
not appear in the [free on-line tutorial][1].

Before you review the code, please note that these programs should be solved
using only techniques seen up to the chapter that the suggested exercises
appear in the book/tutorial.

Exercises list:
---------------

- **Fix up the ask method**
  That ask method I showed you was alright, but I bet you could do better. Try
  to clean it up by removing the variables `good_answer` and `answer`. You'll
  have to use `return` to exit from the loop. (Well, it will get you out of
  the whole method, but it will get you out of the loop in the process.) How
  do you like the resulting method? I usually try to avoid using `return`
  (personal preference), but I might make an exception here.
- **Old-school Roman numerals**
  In the early days of Roman numerals, the Romans didn't bother with any of
  this new-fangled subtraction “IX” nonsense. No sir, it was straight addition,
  biggest to littlest —so 9 was written “VIIII”, and so on. Write a method
  that, when passed an integer between 1 and 3000 (or so), returns a string
  containing the proper old-school Roman numeral. In other words,
  `old_roman_numeral 4` should return `IIII`. Make sure to test your method on
  a bunch of different numbers. Hint: Use the integer division and modulus
  methods on page 36.
  For reference, these are the values of the letters used:

    > I = 1, C = 100, V = 5, D = 500, X = 10, M = 1000, L = 50

- **“Modern” Roman numerals**
  Eventually, someone thought it would be terribly clever if putting a smaller
  number before a larger one meant you had to subtract the smaller one. As a
  result of this development, you must now suffer. Rewrite your previous method
  to return the new-style Roman numerals, so when someone calls
  `roman_numeral 4` , it should return `IV`.
- **Fix Map Count**
  Get `map_count` to work when it measures the size of a continent that touches
  a border of the map.
- **Iterative List Sorting**
  Write your own sorting method. So we want to sort an array of words, and we
  know how to find out which of two words comes first in the dictionary (using
  `<` ). What strikes me as probably the easiest way to do this is to keep two
  more lists around: one will be our list of already-sorted words, and the
  other will be our list of still-unsorted words. We’ll take our list of words,
  find the “smallest” word (that is, the word that would come first in the
  dictionary), and stick it at the end of the already-sorted list. All of the
  other words go into the still-unsorted list. Then you do the same thing
  again but using the still-unsorted list instead of your original list: find
  the smallest word, move it to the sorted list, and move the rest to the
  unsorted list. Keep going until your still-unsorted list is empty.
- **Recursive List Sorting**
  Write your own sorting method, now using recursion.
- **List Suffle**
  Now that you’ve finished your new sorting algorithm, how about the opposite?
  Write a `shuffle` method that takes an array and returns a totally shuffled
  version. As always, you’ll want to test it, but testing this one is
  trickier... How can you test to make sure you are getting a perfect shuffle?
  What would you even say a perfect shuffle would be? Now test for it.
- **Dictionary Sort**
  Your sorting algorithm is pretty good, sure. But there was always that sort
  of embarrassing point you were hoping I’d just sort of gloss over, right?
  About the capital letters? Your sorting algorithm is good for general-
  purpose sorting, but when you sort strings, you are using the ordering of the
  characters in your fonts (called the ASCII codes) rather than true dictionary
  ordering. In a dictionary, case (upper or lower) is irrelevant to the
  ordering. So make a new method to sort words (something like
  `dictionary_sort`). Remember, though, that if I give your program words
  starting with capital letters, it should return words with those same capital
  letters, just ordered as you’d find in a dictionary.
- **File move/rename**
  Adapt the picture-downloading/file-renaming program to your computer. Add some
  safety features to make sure you never overwrite a file. A few methods you
  might find useful are `File.exist?` (pass it a filename, and it will return
  `true` or `false` ) and `exit` (like if `return` and Napoleon had a baby —it
  kills your program right where it stands; this is good for spitting out an
  error message and then quitting).
- **Playlist Builder**
  Build your own playlists! For this to work, you need to have some music ripped
  to your computer in some format. Building a playlist is easy. It’s just a
  regular text file (no YAML required, even). Each line is a filename. What 
  makes it a playlist? Well, you have to give the file the .m3u extension, like
  playlist.m3u or something. And that’s all a playlist is: a text file with a 
  .m3u extension. So have your program search for various music files and build
  you a playlist. Use your `shuffle` method on page 85 to mix up your playlist.
  Then check it out in your favorite music player (Winamp, MPlayer, etc.)!

    > We’ve ripped a 100 or so CDs, and we keep them in directories something
    > like:
    > 
    > music/genre/artist_and_cd_name/track_number.ogg
    > 
    > (I’m partial to the .ogg format, though this would work just as well with
    > .mp3s or whatever you use.). Then, every line in the playlist should be
    > something like:
    > 
    > music/world/Stereolab--Margarine_Eclipse/track05.ogg
   
- **Playlist Mixer**
  Build better playlists! After listening to playlists for a while, you might
  start to find that a purely random shuffle just doesn’t quite seem... mixed up
  enough. So here’s the grand challenge: instead of using your old
  shuffle , write a new music_shuffle method. It should take an array of
  filenames (like those listed previously) and mix them up good and proper. Mix
  it up as best you can!
- **Roman to Integer**
  Time to party like it’s `roman_to_integer 'mcmxcix'`! Come on, you knew it was
  coming, didn’t you? It’s the other half of your `int_to_roman 1999` method.
  Make sure to reject strings that aren’t valid Roman numerals.
- **Birthday Helper**
  Birthday helper! Write a program to read in names and birth dates from a text
  file. It should then ask you for a name. You type one in, and it tells you
  when that person’s next birthday will be (and, for the truly adventurous, how
  old they will be). The input file should look something like this:

    > Christopher Alexander, Oct 4, 1936
    > 
    > Christopher Lambert, Mar 29, 1957
    > 
    > Christopher Lee, May 27, 1922

- **Change a Class**
  How about making your `shuffle` method on page 85 an array method? Or making
  `factorial` an integer method? `4.to_roman`, anyone? In each case, remember to
  use `self` to access the object the method is being called on (the 4 in
  `4.to_roman`).
- **Cheating On Dices**
  Our dice are just about perfect. The only feature that might be missing is a
  way to set which side of a die is showing...why don’t you write a `cheat`
  method that does just that? Come back when you’re done (and when you tested
  that it worked, of course). Make sure that someone can’t set the die to have
  a 7 showing; you’re cheating, not bending the laws of logic.
- **Even Better Profiling**
  Modify the profile method so you can turn all profiling on and off by changing
  just one line of code. Just one word!

---
**Note:** The description of the problems is &copy; Chris Pine and are included
under _Fair Use_. Unless otherwise stated, all the code included in this
repository is released to the public domain under the [Creative Commons CC0
1.0 Universal License][CC].


  [1]: https://github.com/Rojo/Respuestas-de-Aprende-a-Programar
  [2]: https://pragprog.com/book/ltp2/learn-to-program
  [CC]: https://creativecommons.org/publicdomain/zero/1.0/legalcode
