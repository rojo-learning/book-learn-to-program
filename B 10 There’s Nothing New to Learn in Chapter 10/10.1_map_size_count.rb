
# These are just to make the map easier for me to read. "M" is visually more
# dense than "o"
M = 'land'
o = 'water'

world = [
  [o,o,o,o,o,o,o,o,o,o,o],
  [o,o,o,o,M,M,o,o,o,o,o],
  [o,o,o,o,o,o,o,o,M,M,o],
  [o,o,o,M,o,o,o,o,o,M,o],
  [o,o,o,M,o,M,M,o,o,o,o],
  [o,o,o,o,M,M,M,M,o,o,o],
  [o,o,o,M,M,M,M,M,M,M,o],
  [o,o,o,M,M,o,M,M,M,o,o],
  [o,o,o,o,o,o,M,M,o,o,o],
  [o,M,o,o,o,M,o,o,o,o,o],
  [o,o,o,o,o,o,o,o,o,o,o]
]

def continent_size world, x, y
  if world[y][x] != 'land'
    # Either it ' s water or we already counted it, but either way, we don ' t
    # want to count it now.
    return 0
  end

  # So first we count this tile...
  size = 1
  world[y][x] = 'counted land'
  # ...then we count all of the neighboring eight tiles (and, of course, their
  # neighbors by way of the recursion).
  size = size + continent_size(world, x-1, y-1)
  size = size + continent_size(world,   x, y-1)
  size = size + continent_size(world, x+1, y-1)
  size = size + continent_size(world, x-1,   y)
  size = size + continent_size(world, x+1,   y)
  size = size + continent_size(world, x-1, y+1)
  size = size + continent_size(world,   x, y+1)
  size = size + continent_size(world, x+1, y+1)

  size
end

# Well, there is actually one small bug for you to fix. This code works fine
# because the continent does not border the edge of the world. If it did, then
# when we send our little guys out (that is, call continent_size on a new tile),
# some of them would fall off the edge of the world (that is, call
# continent_size with invalid values for x and/or y ), which would probably
# crash on the very first line of the method.
#
# It seems like the obvious way to fix this would be to do a check before each
# call to continent_size (sort of like sending your little guys out only if they
# aren’t going to fall over the edge of the world), but that would require eight
# separate (yet nearly identical) checks in your method. Yuck. It would be
# lazier to just send your guys out, and if they fall off the edge of the world,
# have them shout back “ZERO!” (In other words, put the check right at the top
# of the method, very much like the check we put in to see whether the tile was
# uncounted land.) So go for it!

################################################################################

# For Ruby 1.8.6 onwards, trying to get a non existen element in an array will
# return «nil», this means that the code works without changes as the test below
# shows with a continent that touches the four borders of the map.

# I suspect that for Ruby 1.8.2, world[y][x] would need to be explicity tested
# for «nil» values in order to avoid an error in the program when a continent
# touches a border of the map.

new_world = [
  [o,o,M,M,o,o,o,o,o,o,o],
  [o,o,o,M,M,M,o,o,o,o,o],
  [o,o,o,M,o,o,o,o,M,M,o],
  [o,o,o,M,o,o,o,o,o,M,o],
  [o,o,o,M,o,M,M,o,o,o,o],
  [o,o,o,o,M,M,M,M,o,o,o],
  [o,o,o,M,M,M,M,M,M,M,M],
  [M,M,o,M,M,o,M,M,M,o,M],
  [M,M,M,o,o,o,M,M,o,o,o],
  [o,M,o,o,o,M,o,o,o,o,o],
  [o,o,o,o,M,o,o,o,o,o,o]
]

puts continent_size(world, 5, 5)
puts continent_size(new_world, 5, 5)

__END__

Chris Pine holds the rights for the code of the «continent_size» function.
