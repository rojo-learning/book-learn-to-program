
# Write your own sorting method.
# So we want to sort an array of words, and we know how to find out which of two
# words comes first in the dictionary (using < ). What strikes me as probably
# the easiest way to do this is to keep two more lists around: one will be our
# list of already-sorted words, and the other will be our list of still-unsorted
# words. We’ll take our list of words, find the “smallest” word (that is, the
# word that would come first in the dictionary), and stick it at the end of the
# already-sorted list. All of the other words go into the still-unsorted list.
# Then you do the same thing again but using the still-unsorted list instead of
# your original list: find the smallest word, move it to the sorted list, and
# move the rest to the unsorted list. Keep going until your still-unsorted list
# is empty.

# Finds the smallest numeric or text element in an array, comparing the
# elements using the `<` method.
#
# list - Array of strings or numbers.
#
# Examples
#
#   get_smallest([-2, -1, 0, 1, 2])
#   # => -2
#   get_smallest(['a','A','b','B','c','C'])
#   # => 'A'
#
# Returns the smallest element or nil.
def get_smallest(list)
  # Get the first item and set it as the «smallest»
  smallest = list[0]

  index = 1
  limit = list.length

  # Cycle through the elements in the list (skipping the first one) to see if
  # there is a smaller item. If one is found, set it as the «smallest».
  while index < limit
    if list[index] < smallest
      smallest = list[index]
    end

    index = index + 1
  end

  smallest
end

# Searches for the given element (number or string) into the given list of items
# of the same type, if it exists, returns a version of the list without that
# element.
#
# element - Number or string.
# list    - Array of numbers or strings.
#
# Examples
#
#   remove_element(2, [1, 2, 3])
#   # => [1, 3]
#   remove_element(4, [1, 2, 3])
#   # => [1, 2, 3]
#
# Returns the given array without the given element.
def remove_element(element, list)
  helper = []
  index  = 0
  limit  = list.length

  # Cycle through «list», pushing every element into the «helper», except
  # for the «element» element.
  while index < limit
    if list[index] == element
      element == nil
    else
      helper.push list[index]
    end

    index = index + 1
  end

  helper
end

# Sorts a given list of string or integers applying the `<` operator.
#
# list - Array of numbers or strings.
#
# Examples
#
#   iterative_sort(['a', 'B', 'e', 'A'])
#   # => ['A', 'B', 'a', 'e']
#   iterative_sort([3, 2, 1, 0])
#   # => []
#
# Returns a sorted version of the list.
def iterative_sort list
  unsorted = list
  sorted   = []

  while unsorted.length > 0
    smallest = get_smallest(unsorted)
    unsorted = remove_element(smallest, unsorted)
    sorted.push smallest
  end

  sorted
end

puts iterative_sort("Somos todo lo que no se ve".split)
