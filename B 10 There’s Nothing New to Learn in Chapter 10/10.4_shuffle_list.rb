
# Now that you’ve finished your new sorting algorithm, how about the opposite?
# Write a shuffle method that takes an array and returns a totally shuffled
# version. As always, you’ll want to test it, but testing this one is
# trickier...
# How can you test to make sure you are getting a perfect shuffle? What would
# you even say a perfect shuffle would be? Now test for it.

# Searches for the given element (number or string) into the given list of items
# of the same type, if it exists, returns a version of the list without that
# element.
#
# element - Number or string.
# list    - Array of numbers or strings.
#
# Examples
#
#   remove_element(2, [1, 2, 3])
#   # => [1, 3]
#   remove_element(4, [1, 2, 3])
#   # => [1, 2, 3]
#
# Returns the given array without the given element.
def remove_element(element, list)
  helper = []
  index  = 0
  limit  = list.length

  # Cycle through «list», pushing every element into the «helper», except
  # for the «element» element.
  while index < limit
    if list[index] == element
      element == nil
    else
      helper.push list[index]
    end

    index = index + 1
  end

  helper
end

# Creates a suffled version of the given list that contains numbers or strings.
#
# list - Array of numbers or strings.
#
# Examples
#
#   suffle([1, 2, 3, 4, 5])
#   # => [2, 4, 1, 5, 3]
#   suffle([1, 2, 3, 4, 5])
#   # => [1, 5, 3, 4, 2]
#
# Returns a suffled version of the given array.
def suffle(list)
  suffled = []

  # Cycle while the «list» has elements, picking a random «element» each time.
  # That «element» is appended to the «suffled» version and is removed from the
  # «list».
  while list.length > 0
    if list.length == 1
      element = list[0]
    else
      element = list[rand(list.length)]
    end

    suffled.push(element)
    list = remove_element(element, list)
  end

  suffled
end

arry = [1, 2, 3, 4, 5]
p suffle(arry)
p suffle(arry)
p suffle(arry)
p suffle(arry)
p suffle(arry)
