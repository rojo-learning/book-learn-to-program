
# Your sorting algorithm is pretty good, sure. But there was always that sort of
# embarrassing point you were hoping I’d just sort of gloss over, right? About
# the capital letters? Your sorting algorithm is good for general-purpose
# sorting, but when you sort strings, you are using the ordering of the
# characters in your fonts (called the ASCII codes) rather than true dictionary
# ordering. In a dictionary, case (upper or lower) is irrelevant to the
# ordering. So make a new method to sort words (something like dictionary_sort).
# Remember, though, that if I give your program words starting with capital
# letters, it should return words with those same capital letters, just ordered
# as you’d find in a dictionary.

# Finds the smallest text element in an array, comparing the elements using the
# `<` method. This is case insensitive.
#
# list - Array of strings.
#
# Examples
#
#   get_smallest(['a','A','b','B','c','C'])
#   # => 'a'
#   get_smallest(['b','C','d'])
#   # => 'b'
#
# Returns the smallest element or nil.
def get_smallest(list)
  # Get the first item and set it as the «smallest»
  smallest = list[0]

  index = 1
  limit = list.length

  # Cycle through the elements in the list (skipping the first one) to see if
  # there is a smaller item, using capitalize to make the process case
  # insensitive. If one is found, set it as the «smallest».
  while index < limit
    if list[index].capitalize < smallest.capitalize
      smallest = list[index]
    end

    index = index + 1
  end

  smallest
end

# Searches for the given string into the given list of items of the same type,
# if it exists, returns a version of the list without that element.
#
# element - string.
# list    - Array of strings.
#
# Examples
#
#   remove_element('a', ['a', 'b', 'c'])
#   # => ['b', 'c']
#   remove_element('d', ['a', 'b', 'c'])
#   # => ['a', 'b', 'c']
#
# Returns the given array without the given element.
def remove_element(element, list)
  helper = []
  index  = 0
  limit  = list.length

  # Cycle through «list», pushing every element into the «helper», except
  # for the «element» element.
  while index < limit
    if list[index] == element
      element == nil
    else
      helper.push list[index]
    end

    index = index + 1
  end

  helper
end

# Sorts a given list of strings, applying the `<` operator. It is case
# insensitive, so the strings will be sorted as they appear in a dictionary.
#
# list - Array of strings.
#
# Examples
#
#   dictionary_sort(['a', 'B', 'e', 'A'])
#   # => ['a', 'A', 'B', 'e']
#
# Returns a sorted version of the list.
def dictionary_sort list
  unsorted = list
  sorted   = []

  while unsorted.length > 0
    smallest = get_smallest(unsorted)
    unsorted = remove_element(smallest, unsorted)
    sorted.push smallest
  end

  sorted
end

puts iterative_sort("Somos todo lo que no se ve".split)
