
# Write your own sorting method, now using recursion.
# With the recursion, you might need a wrapper method, a tiny method that wraps
# up another method into a cute little package, like this:
#
# def sort some_array # This "wraps" recursive_sort.
#   recursive_sort some_array, []
# end
#
# def recursive_sort unsorted_array, sorted_array
#   Your fabulous code goes here.
# end
#
# What was the point of the wrapper method? Well, recursive_sort took two
# parameters, but if you were just trying to sort an array, you would always
# have to pass in an empty array as the second parameter. This is a silly thing
# to have to remember. Here, the wrapper method passes it in for us, so we never
# have to think about it again.

# Finds the smallest numeric or text element in an array, comparing the
# elements using the `<` method.
#
# list - Array of strings or numbers.
#
# Examples
#
#   get_smallest([-2, -1, 0, 1, 2])
#   # => -2
#   get_smallest(['a','A','b','B','c','C'])
#   # => 'A'
#
# Returns the smallest element or nil.
def get_smallest(list)
  # Get the first item and set it as the «smallest»
  smallest = list[0]

  index = 1
  limit = list.length

  # Cycle through the elements in the list (skipping the first one) to see if
  # there is a smaller item. If one is found, set it as the «smallest».
  while index < limit
    if list[index] < smallest
      smallest = list[index]
    end

    index = index + 1
  end

  smallest
end

# Searches for the given element (number or string) into the given list of items
# of the same type, if it exists, returns a version of the list without that
# element.
#
# element - Number or string.
# list    - Array of numbers or strings.
#
# Examples
#
#   remove_element(2, [1, 2, 3])
#   # => [1, 3]
#   remove_element(4, [1, 2, 3])
#   # => [1, 2, 3]
#
# Returns the given array without the given element.
def remove_element(element, list)
  helper = []
  index  = 0
  limit  = list.length

  # Cycle through «list», pushing every element into the «helper», except
  # for the «element» element.
  while index < limit
    if list[index] == element
      element == nil
    else
      helper.push list[index]
    end

    index = index + 1
  end

  helper
end

# Sorts a given list of string or integers applying the `<` operator.
#
# list - Array of numbers or strings.
#
# Examples
#
#   recursive_sort(['a', 'B', 'e', 'A'])
#   # => ['A', 'B', 'a', 'e']
#   recursive_sort([3, 2, 1, 0])
#   # => []
#
# Returns a sorted version of the list.
def recursive_sort(list)
  if list.length == 0
    return []
  end

  smaller = get_smallest(list)
  [smaller] + recursive_sort(remove_element(smaller, list))
end

puts recursive_sort("Somos todo lo que no se ve".split)
